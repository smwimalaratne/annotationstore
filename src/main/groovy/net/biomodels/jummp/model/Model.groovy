/**
 * Copyright (C) 2010-2014 EMBL-European Bioinformatics Institute (EMBL-EBI),
 * Deutsches Krebsforschungszentrum (DKFZ)
 *
 * This file is part of Jummp.
 *
 * Jummp is free software; you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * Jummp is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along
 * with Jummp; if not, see <http://www.gnu.org/licenses/agpl-3.0.html>.
 **/

package net.biomodels.jummp.model

import grails.persistence.Entity
import org.apache.commons.logging.LogFactory
import org.apache.commons.logging.Log

/**
 * @short Representation of one Model.
 * This class is the representation of one Model. It contains the reference
 * to the model file stored in the version control system and the references
 * to the meta information such as publications and the list of revisions of
 * the Model.
 * The Model is the central domain class of Jummp.
 * @see Revision
 * @see Publication
 * @author Martin Gräßlin <m.graesslin@dkfz-heidelberg.de>
 * @author Mihai Glonț <mihai.glont@ebi.ac.uk>
 */
@Entity
class Model implements Serializable {
    private static final long serialVersionUID = 1L
    /* the class logger */
    private static final Log log = LogFactory.getLog(this)
    /* semaphore for the log threshold */
    private static final boolean IS_INFO_ENABLED = log.isInfoEnabled()
    /**
     * A Model has many Revision
     * IMPORTANT: never access revisions directly as this circumvents the ACL!
     * Use ModelService.getAllRevisions()
     */
    static hasMany = [revisions: Revision]
    /**
     * The path, relative to the folder containing all models,
     * of the folder dedicated to this model
     */
    String vcsIdentifier
    /**
     * The Publication the model has been described in
     */
    Publication publication
    /*
     * Whether the model has been deleted
    */
    boolean deleted = false
    /**
     * The perennial submission identifier of this model.
     */
    String submissionId
    /**
     * The perennial publication identifier of this model.
     */
    String publicationId
    /**
     * Date when a version of this model first became public.
     */
     Date firstPublished

    static mapping = {
        publication lazy: false
        // publication_id is already taken
        publicationId column: 'perennialPublicationIdentifier'
    }

    static constraints = {
        vcsIdentifier(nullable: false, blank: false, unique: true)
        revisions(nullable: false, validator: { revs ->
            return !revs.isEmpty()
        })
        publication(nullable: true)
        deleted(nullable: false)
        submissionId blank: false, unique: true
        publicationId nullable: true
        firstPublished nullable: true
    }
}
