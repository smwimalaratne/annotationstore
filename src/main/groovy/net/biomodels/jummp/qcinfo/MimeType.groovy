package net.biomodels.jummp.qcinfo

/**
 * Created by sarala on 06/07/2016.
 * Created by tung on 06/07/2016.
 */
enum MimeType {

    PDF,

    TEXT
}

