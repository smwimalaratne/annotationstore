package net.biomodels.jummp.qcinfo

/**
 * Created by sarala on 06/07/2016.
 * Created by tung on 06/07/2016.
 */

import grails.persistence.Entity
import net.biomodels.jummp.model.Revision

@Entity
class QcInfo implements Serializable{

    FlagLevel flag
    String comment
/*    String reportName
    String reportContent
    MimeType reportType*/

/*
    static mapping = {
        reportContent type: 'blob'
    }
*/

    static constraints = {
        comment(maxSize: 1000)
        flag(nullable: true, blank: false, unique: false)
    }
}
