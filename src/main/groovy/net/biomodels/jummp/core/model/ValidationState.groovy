package net.biomodels.jummp.core.model

/**
 * Created by IntelliJ IDEA.
 *
 * @author Sarala Wimalaratne
 *         Date: 03/11/2015
 *         Time: 10:50
 */
public enum ValidationState {
    /*Waiting to be approved*/
    APPROVE,

    /*Metadata Approved*/
    APPROVED,

    /*Metada invalid - rejected*/
    REJECTED,

    /*Metadata fields missing*/
    CONDITIONALLY_APPROVED
}
